/* Name: Ryan McMahon */

// TODO document this
var board = document.getElementsByTagName("td");

// array for all possible winning combinations
var winSets = [
  // horizontal wins
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  // vertical wins
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  // diagonal wins
  [0, 4, 8],
  [2, 4, 6]
];

// X always gets to go first
// empty array for playerX, playerO and clickedNumbers
// game over is equal to false when game starts
var currentPlayer = "X";
var playerX = [];
var playerO = [];
var clickedNumbers = [];
var isGameOver = false;

// reloads page to reset game
function resetGame() {
  window.location.reload();
}

 // cell is event target
 // if statement to disable the ability to insert more than one value in a cell during and after the game is over
 // pushing cell number values into clickedNumbers array "bucket"
 // if statement for playerX, push clicked cell number value into playerX array "bucket", playerX owns that cell
 // checkWin function and then switch players
 // else if statement for playerO, push clicked cell number value into playerO array "bucket", playerO owns that cell
 // checkWin function and then switch players
function cellClicked() {

  let cell = event.target;
    let player = document.querySelector("#player");
    let number = Number(cell.getAttribute("number"));
    if (clickedNumbers.includes(number) || isGameOver == true){
      return;
    }
    clickedNumbers.push(number);
    let playerValue = player.innerHTML;
    cell.innerHTML = currentPlayer;
    if(playerValue == "X"){
      playerX.push(number);
      checkWin();
      currentPlayer = "O";
    }
    else if (playerValue == "O"){
      playerO.push(number);
      checkWin();
      currentPlayer = "X";
    };
    player.innerHTML = currentPlayer;
}

// Function checkWin() is called to check all winning combinations and display results

// for loop to check array for winning sets
// for loop to check if numbers of a winSet are included in playerX or playerO selections/arrays
// hitCount increases if true
// if hitCount is equal to 3, winSets is completed
// winner message is displayed and game is over
function checkWin() {
  for(i = 0; i < winSets.length; i++){
      let set = winSets[i];
      let hitCount = 0;
      for(a = 0; a < set.length; a++){
          let number = set[a];
          if(currentPlayer == "X"){
            if(playerX.includes(number)){
              hitCount++;
            };
          }
          else if (currentPlayer == "O"){
            if(playerO.includes(number)){
              hitCount++;
          }
      }
      if(hitCount == 3){
        displayMessage();
        isGameOver = true;
        document.querySelector("#winner").innerHTML = currentPlayer + " is the winner!";
      }
    }
  }
}
// ==========================================================================

let resetButton = document.getElementById("reset")
resetButton.addEventListener("click", resetGame);

let message = document.getElementById("message")
message.addEventListener("click", resetGame);

for ( i = 0; i < board.length; i++) {
    document.getElementsByTagName("td")[i].addEventListener("click", function() {
        cellClicked(this);
    });
}

// displays the results window with the winner inside it
function displayMessage() {
  document.getElementById("message").style.display = "block";
}

// ==========================================================================